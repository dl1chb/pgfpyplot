"""
PgfPyPlot

Copyright 2021 Christopher Beck (beckus@beckus.eu)

This file is part of PgfPyPlot.

    PgfPyPlot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PgfPyPlot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PgfPyPlot.  If not, see <https://www.gnu.org/licenses/>.
"""
from setuptools import setup, find_packages

setup(name='PgfPyPlot',
    version='0.0.2',
    description='library for plotting nice graphs from cadence',
    url='https://gitlab.com/dl1chb/pgfpyplot',
    author='Christopher Beck',
    author_email='beckus@beckus.eu',
    license='GPLv3',
#    packages=['pylabte'],
    install_requires=['numpy', 'pandas'],
    packages=find_packages(),
    namespace_packages=['pgfpyplot'],
    zip_safe=False)
