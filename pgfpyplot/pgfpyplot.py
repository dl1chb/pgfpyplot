#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
PgfPyPlot

Copyright 2021 Christopher Beck (beckus@beckus.eu)

This file is part of PgfPyPlot.

    PgfPyPlot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PgfPyPlot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PgfPyPlot.  If not, see <https://www.gnu.org/licenses/>.

Changelog
  * 2020-07-10 Christopher Beck (beckus@beckus.eu)
      created
"""
import numpy as np
import pandas
from string import Template

class data_obj():
    def __init__(self, data: np.array, name :str, option :str):
        self.data = data
        self.name = name
        self.option = option


class pgfpyplot():
    def __init__(self, name :str):
        self.name = name
        self.max = None
        self.min = None
        self.data_list = []
        self.prefix = ""
        self.xlabel = ""
        self.ylabel = ""
        print ("new plot "+ name+ " initialized")

    def setPrefix(self, prefix : str):
        self.prefix = prefix
        return

    def addData(self, data_array :np.array, data_name :str, option="black"):
#        try:
#            if xaxis.size == data_array.size:
        self.data_list.append(data_obj(data_array, data_name, option))
        if self.max == None:
            self.max=np.max(data_array)
        if np.max(data_array) > self.max:
            self.max = np.max(data_array)
        if self.min == None:
            self.min = np.min(data_array)
            print("min: ",self.min," max: ",self.max)
        if np.min(data_array) < self.min:
            self.min = np.min(data_array)
            print("min: ",self.min," max: ",self.max)
        print("min: ",self.min," max: ",self.max)
        return
    
    def printData(self):
        for obj in self.data_list:
            print ("name: ",obj.name, "\nWert:\n", obj.data, "\nOption:",obj.option, "\n")
            return
            
    def addXaxis(self, axis :np.array, name :str):
        self.xaxis = data_obj(axis, name, "")
        return
    
    def setXlabel(self, xlabel : str):
        self.xlabel = xlabel
        return

    def setYlabel(self, ylabel : str):
        self.ylabel = ylabel
        return
        
    def writeCSV(self):
        print("writing data to "+self.prefix+self.name+".csv")
        data_dict = {self.xaxis.name : self.xaxis.data}
        for obj in self.data_list:
            data_dict.update({obj.name : obj.data})
        df = pandas.DataFrame(data_dict)
        df.to_csv(self.name + ".csv", index=False)
        
    def writePGF(self):
        print("just basics work")
        """
        \addplot [red] table [x=angle, y=gainE, col sep=comma] {daten/antennen/plastik.csv};
        \addlegendentry{meas E-Plane};
        """
        plotting_string = ""
        for obj in self.data_list:
            plotting_string += "\\addplot ["+obj.option+"] table [x="+self.xaxis.name+", y="+obj.name+", col sep = comma] {"+self.prefix+"/"+self.name+".csv};\n"
            plotting_string += "\\addlegendentry{"+obj.name+"};\n"
        template_in = open('pgf_template.txt')
        src_file = Template( template_in.read() )
        replace_dict = {'xmin' : np.min(self.xaxis.data),
                        'xmax' : np.max(self.xaxis.data),
                        'ymin' : self.min,
                        'ymax' : self.max,
                        'xlabel' : self.xlabel,
                        'ylabel' : self.ylabel,
                        'plottingstring' : plotting_string}
        result_string = src_file.substitute(replace_dict)
        result_file = open(self.prefix+"/"+self.name+".pgf", "w+")
        result_file.write(result_string)
        result_file.close()
        print(result_file)