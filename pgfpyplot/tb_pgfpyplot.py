#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
PgfPyPlot

Copyright 2021 Christopher Beck (beckus@beckus.eu)

This file is part of PgfPyPlot.

    PgfPyPlot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PgfPyPlot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PgfPyPlot.  If not, see <https://www.gnu.org/licenses/>.

Changelog
  * 2020-07-10 Christopher Beck (beckus@beckus.eu)
      created
"""

import numpy as np
import sys, os

"""
Add your path to pgfpyplot.py when not installed as package

sys.path.append("/home/beck/Developement/pgfpyplot/pgfpyplot")

And do not forget to copy the pgf_template.txt into the same directory!
This will be improved the next time
"""

from pgfpyplot import pgfpyplot

myplot = pgfpyplot("testplot")

# define the directory where your pgf pictures want to be
myplot.setPrefix(os.getcwd())

x = np.arange(0,10,1)
foo = np.arange(4,24,2)
#bar = np.arange(-6, -1, 0.5)


myplot.addXaxis(x, "frequenz")
myplot.addData(foo, "foo", "red dashed")
#myplot.addData(bar, "bar", "blue")
myplot.printData()
myplot.writeCSV()
myplot.writePGF()